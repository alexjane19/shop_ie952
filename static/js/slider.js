/**
 * Created by alex on 7/1/17.
 */

var  Slider = function () {

    var carousel = function ()  {
        var i;
        var x = document.getElementsByClassName("img-slider");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > x.length) {slideIndex = 1}
        x[slideIndex-1].style.display = "inline";
        setTimeout(carousel, 2000); // Change image every 2 seconds
    }
    var slideIndex = 0;
    carousel();
}