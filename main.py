import tornado.web, json, os, tornado.escape, random,string, time, copy, base64, urllib, tornado.httpserver, \
    tornado.ioloop, tornado.options, tornado.web, tornado.options

from datetime import datetime,timedelta

import webbrowser
from database_handler import DatabaseHandler

class BaseHandler(tornado.web.RequestHandler):
    def get_current_user(self): return self.get_secure_cookie("user")
    def encode_json(self,data):
        return json.dumps(data)
    def decode_json(self,data):
        return json.loads(data)
    def generate_random_string(self,n):
        return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(n))

class MainHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.render("index.html", user=self.application.user)
        else:
            self.application.user = str(self.application.user, encoding='utf-8')
            self.render("index.html", user=self.application.user)

class ProfileHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.redirect("/")
        else:
            self.application.user = str(self.application.user, encoding='utf-8')
            self.render("profile.html", user=self.application.user)


class ChangeHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.redirect("/")
        else:
            self.render("change-password.html")

    def post(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.redirect("/")

        cur_user = str(self.application.user, encoding='utf-8')
        new_pass = self.get_argument("new-password")
        re_pass = self.get_argument("retype-password")
        if re_pass != new_pass:
            return self.write(self.encode_json({"command": "print", "msg": "رمز جدید شما با تکرار آن یکسان نیستند."}));

        self.application.suser = cur_user
        enter = self.application.db_handler.change_password([new_pass, cur_user])
        if enter:
            return self.write(self.encode_json({"command": "redirect", "msg": "successful", "url": "/logout"}));
        else:
            return self.write(self.encode_json({"command": "print", "msg": "در پایگاه داده مشکلی به وجود آمده است."}));

        # self.redirect("/logout")


class ForgetHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.render('forget-password.html', user=self.application.user)
        else:
            self.application.user = str(self.application.user, encoding='utf-8')
            self.redirect("/")

    def post(self):
        self.application.user = self.get_current_user()
        if self.application.user != None:
            return self.redirect("/")

        email = self.get_argument("email")
        if email == "":
            return self.write(self.encode_json({"command": "print", "msg": "ایمیل خود را وارد کنید."}));

        random_string = self.generate_random_string(10)
        url = "http://localhost:8888/update/password?key=" + random_string;
        now_plus_30 = datetime.now() + timedelta(minutes=30)

        enter = self.application.db_handler.forget_password([email, random_string, now_plus_30])
        if enter[0]:
            return self.write(self.encode_json({"command": "forget-password", "msg": enter[1], "url": url}));
        else:
            return self.write(self.encode_json({"command": "print", "msg": enter[1]}));


class UpdatePasswordHandler(BaseHandler):
    def get(self):
        random_string = self.get_argument('key', True)
        self.application.user = self.get_current_user()
        if self.application.user != None:
            return self.redirect("/")

        enter = self.application.db_handler.check_random_string([random_string])
        if enter[0]:
            self.application.email = enter[1]
            return self.render('update-password.html', user=self.application.user, can_do=True)
            # return self.write(self.encode_json({"command": "forget-password", "msg": enter[1]}));
        else:
            return self.render('update-password.html', user=self.application.user, can_do=False, msg=enter[1])
            # return self.write(self.encode_json({"command": "print", "msg": enter[1]}));

    def post(self):
        self.application.user = self.get_current_user()
        if self.application.user != None:
            return self.redirect("/")

        new_pass = self.get_argument("new-password")
        re_pass = self.get_argument("retype-password")
        if re_pass != new_pass:
            return self.write(self.encode_json({"command": "print", "msg": "رمز جدید شما با تکرار آن یکسان نیستند."}));

        enter = self.application.db_handler.update_password([new_pass, self.application.email])
        if enter[0]:
            return self.write(self.encode_json({"command": "redirect", "url": "/login"}));
        else:
            return self.write(self.encode_json({"command": "print", "msg": enter[1]}));


class LoginHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.render('login.html', user=self.application.user, suser=self.application.suser)
        else:
            self.application.user = str(self.application.user , encoding='utf-8')
            self.redirect("/")

    def post(self):
        user1 = self.get_argument("username")
        pass1 = self.get_argument("password")
        enter = self.application.db_handler.login([user1, pass1])
        if enter:
            self.set_secure_cookie("user", user1)
            self.redirect("/")
        else:
            self.application.suser = user1
            self.redirect("/login")


class SignUpHandler(BaseHandler):
    # def get(self):
    #     self.render('login.html')

    def post(self):
        user1 = self.get_argument("username")
        pass1 = self.get_argument("password")
        email = self.get_argument("email")
        if user1 != "" and pass1 != "" and email != "":
            if self.application.db_handler.sign_up([user1, email, pass1]):
                self.application.suser = user1
                return self.redirect("/login")


class LogoutHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.clear_cookie("user")
        self.clear_all_cookies()
        self.redirect("/login")


class StuffHandler(BaseHandler):
    def get(self,category,page):
        self.application.user = self.get_current_user()
        ajax = self.get_argument('ajax', True)
        products = self.application.db_handler.get_products([category, (int(page)-1)*10])
        url_product = "/product/";
        url_picture_product = "/static/img/stuff/";
        if ajax:
            return self.render('stuffs.html',user=self.application.user, is_exist=products[0], product=products[1], purl=url_product, picurl=url_picture_product)
        return self.write(self.encode_json({"command": "fetch", "msg": products[1], "purl": url_product , "picurl": url_picture_product}));

class SpecificStuffHandler(BaseHandler):
    def get(self,id):
        self.application.user = self.get_current_user()
        ajax = self.get_argument('ajax', True)
        product = self.application.db_handler.get_product([id])
        url_product = "/product/";
        url_picture_product = "/static/img/stuff/";
        if ajax:
            return self.render('goods_specifications.html',user=self.application.user, is_exist=product[0], product=product[1], purl=url_product, picurl=url_picture_product)
        return self.write(self.encode_json({"command": "fetch", "msg": product[1], "purl": url_product , "picurl": url_picture_product}));


class CategoriesHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        ajax = self.get_argument('ajax', True)
        categories = self.application.db_handler.get_categories();
        if ajax:
            return self.render('search.html',user=self.application.user, categories=categories[1])
        return self.write(self.encode_json({"command": "fetch", "msg": categories[1]}));


class SearchStuffHandler(BaseHandler):
    def get(self,keysearch,page):
        self.application.user = self.get_current_user()
        param_search = self.get_argument('search', True)
        if param_search == '1':
            products = self.application.db_handler.search_by_name([keysearch, (int(page)-1)*10])
        elif param_search == '2':
            products = self.application.db_handler.search_by_category([keysearch, (int(page)-1)*10])
        else:
            return self.write("شما مجاز به انجام این عملیات نیستید");

        url_product = "/product/";
        url_picture_product = "/static/img/stuff/";
        return self.write(self.encode_json({"command": "fetch", "msg": products[1], "purl": url_product , "picurl": url_picture_product}));


class AdminHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            self.redirect("/")
        else:
            self.application.user = str(self.application.user, encoding='utf-8')
            self.render("admin-shop.html", user=self.application.user)

# class AdminProductHandler(BaseHandler):
#     def get(self):
#         self.application.user = self.get_current_user()
#         if self.application.user == None:
#             return self.write("شما مجاز به انجام این عملیات نیستید.");
#         products = self.application.db_handler.get_all_products()
#         return self.write(self.encode_json({"command": "fetch-admin", "msg": products[1]}));


class AdminProductHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        products = self.application.db_handler.get_all_products()
        return self.render("all-products.html", user=self.application.user, products=products[1])
    def post(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("name"), self.get_argument("category"),
                 self.get_argument("price"), 'mouse.png',
                 self.get_argument("color"), self.get_argument("weight"),
                 self.get_argument("warranty"), self.get_argument("manufacturer"),
                 self.get_argument("year_pro"), self.get_argument("description")]

        enter = self.application.db_handler.add_product(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));
    def put(self, *args, **kwargs):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("name"), self.get_argument("category"),
                 self.get_argument("price"), 'mouse.png',
                 self.get_argument("color"), self.get_argument("weight"),
                 self.get_argument("warranty"), self.get_argument("manufacturer"),
                 self.get_argument("year_pro"), self.get_argument("description"), self.get_argument("code-pro")]
        enter = self.application.db_handler.update_product(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));
    def delete(self, *args, **kwargs):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("code-pro")]

        enter = self.application.db_handler.delete_product(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));

class AdminCategoriesHandler(BaseHandler):
    def get(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        categories = self.application.db_handler.get_categories()
        return self.render("all-categories.html", user=self.application.user, categories=categories[1])
    def post(self):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("name"), '']

        enter = self.application.db_handler.add_category(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));
    def put(self, *args, **kwargs):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("name"), '']
        enter = self.application.db_handler.update_category(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));
    def delete(self, *args, **kwargs):
        self.application.user = self.get_current_user()
        if self.application.user == None:
            return self.write("شما مجاز به انجام این عملیات نیستید.");
        param = [self.get_argument("name")]

        enter = self.application.db_handler.delete_category(param)
        return self.write(self.encode_json({"command": "print", "msg": enter[1]}));



class sendHtmls(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        return self.render('authenticated.html')



settings = {
    "cookie_secret": "my secret key",
    "login_url": "/login",
    "template_path": os.path.join(os.path.dirname(__file__), "template"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
}


def set_app_handlers_settings():
    return [tornado.web.url(r'/', MainHandler),
            tornado.web.url(r'/login', LoginHandler),
            tornado.web.url(r'/stuffs/(.*)/([0-9]+)', StuffHandler),
            tornado.web.url(r'/signup', SignUpHandler),
            tornado.web.url(r'/logout', LogoutHandler),
            tornado.web.url(r'/profile', ProfileHandler),
            tornado.web.url(r'/change', ChangeHandler),
            tornado.web.url(r'/forget', ForgetHandler),
            tornado.web.url(r'/update/password', UpdatePasswordHandler),
            tornado.web.url(r'/product/([0-9]+)', SpecificStuffHandler),
            tornado.web.url(r'/search', CategoriesHandler),
            tornado.web.url(r'/search/(.*)/([0-9]+)', SearchStuffHandler),
            tornado.web.url(r'/admin', AdminHandler),
            tornado.web.url(r'/admin/products', AdminProductHandler),
            tornado.web.url(r'/admin/category', AdminCategoriesHandler),

            # tornado.web.url(r'/authenticated', sendHtmls, name="htmls"),
            ]


tornado.options.define("port", default=8888, help="run on the given port", type=int)


def main():
    tornado.options.parse_command_line()
    app = tornado.web.Application(set_app_handlers_settings(), **settings)
    app.debug = True
    app.autoreload = True

    app.db_handler = DatabaseHandler()
    # app.db_handler.create_tables()
    app.user = ""
    app.suser = "" #for username filed in login
    ioloop = tornado.ioloop.IOLoop.instance()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(tornado.options.options.port)
    ioloop.start()


if __name__ == "__main__":
    webbrowser.open('http://localhost:8888/', new=2)
    main()
