/**
 * Created by alex on 7/4/17.
 */
var keysearch;
function prepareStuffsPage() {
    let page = window.location.pathname.slice(window.location.pathname.length-1, window.location.pathname.length);


    try{
        if(page == 1){
            document.getElementById("btn-back").style.visibility = "hidden";
        }else{
            document.getElementById("btn-back").style.visibility = "visible";
        }
    }catch (TypeError){
        // console.log(TypeError.message)
    }

}
function getUrlRequestStuffs() {
    return window.location.pathname.slice(0, window.location.pathname.length-1);

}

function createItemResultHtml(values){
                return '<li> <div class="buys-item"> ' +
                    '<a href="' + values[0] + values[1] + '">' +
                    '<img src="' + values[2] + values[3] + '">' +
                    '<b>' + values[4] + '</b>' +
                    '<span>' + values[5] + '</span></a></div></li>';
}

function setResultSearchInHtml(response, page, callback) {
    let title = "نتیجه";
    if(keysearch[1] == "1"){
        title =  "نتیجه جستجو";
    }else{
        title = keysearch[0]
    }
    var template_html_result1 = '<h3 id="title-search-result">'+title+'</h3>' +
        ' <div class="main-content">' +
        '<div class="content-stuff">' +
        '<ul> ';

        var template_html_result2 = '</ul>' +
        '</div>' +
        '<div class="bar-stuff">' +
        '<i id="btn-back" style="visibility: hidden" class="fa fa-angle-right" onclick="'+ "javascript:backPageSearchClick(document.getElementById('page-number'));"+ '"></i>'  +
    '<p id="page-number">' + page + '</p>' +
    '<i id="btn-next" class="fa fa-angle-left" onclick="'+ "javascript:nextPageSearchClick(document.getElementById('page-number'));"+ '"></i>' +
    '</div>' +
    '</div>';

var template_html_no_result = '<h3 id="title-search-result">'+title+'</h3>' +
        ' <div class="main-content">'   +  '<p class="message-normal">' +
    response[0] + '</p>'+ '</div>';

    var contentHtml = "";
        try {
            console.log(response[0]);

            response[0].forEach(function (item) {
                 contentHtml += createItemResultHtml([response[1], item[0],response[2], item[4], item[1], item[3]])
            });
            callback(template_html_result1 + contentHtml + template_html_result2);

        }catch(TypeError)
        {
            console.log(TypeError)
            if(page == 1)
                callback(template_html_no_result);
            else
                document.getElementById("btn-next").style.visibility = "hidden";

        }


}
function nextPageClick(object) {
    var page = object.innerHTML;
        console.log(page)

    page++;
        console.log(page)

    let url = getUrlRequestStuffs() + page + "?ajax";

    sendHTTPRequest('GET', url,function (response) {
        var contentHtml = "";
        try {
            response[0].forEach(function (item) {

                contentHtml += '<li> <div class="buys-item"> ' +
                    '<a href="' + response[1] + item[0] + '">' +
                    '<img src="' + response[2] + item[4] + '">' +
                    '<b>' + item[1] + '</b>' +
                    '<span>' + item[3] + '</span></a></div></li>';
            });
            object.innerHTML = page;
            document.getElementById("btn-back").style.visibility = "visible";
            document.getElementById("stuffs-contents").innerHTML = contentHtml;
            window.scrollTo(0, 0);

        }catch(TypeError)
        {
            document.getElementById("btn-next").style.visibility = "hidden";
        }
    });

}

function backPageClick(object) {
    var page = object.innerHTML;
    console.log(page)
    page--;
    console.log(page)


    let url = getUrlRequestStuffs() + page + "?ajax";

    sendHTTPRequest('GET', url,function (response) {
        var contentHtml = "";

        response[0].forEach(function (item) {

            contentHtml += '<li> <div class="buys-item"> ' +
                '<a href="' + response[1] + item[0] + '">' +
                '<img src="' + response[2] + item[4] + '">' +
                '<b>' + item[1] + '</b>' +
                '<span>' + item[3] + '</span></a></div></li>';
        });
        document.getElementById("btn-next").style.visibility = "visible";

        object.innerHTML = page

    if(page == 1) {
        document.getElementById("btn-back").style.visibility = "hidden";
    }
        window.scrollTo(0, 0);
        document.getElementById("stuffs-contents").innerHTML = contentHtml;
    });

}

function nextPageSearchClick(object) {
     var page;
    if(object!=null) {
        page =object.innerHTML;
    }else{
        page = 0;
    }
    page++;

    let url = window.location.pathname +"/" +keysearch[0] +"/"+ page +"?search=" + keysearch[1];
console.log(url);
    sendHTTPRequest('GET', url,function (response) {
        setResultSearchInHtml(response,page, function (callback) {
            document.getElementById("search-result-container").innerHTML = callback;
            window.scrollTo(0, 0);
              if(object!=null) {
                   document.getElementById("btn-back").style.visibility = "visible";
              }

        });
    });

}

function backPageSearchClick(object) {
    var page;
    if(object!=null) {
        page =object.innerHTML;
    }else{
        page = 0;
    }
    page--;

 let url = window.location.pathname +"/" +keysearch[0] +"/"+ page +"?search=" + keysearch[1];
console.log(url);

    sendHTTPRequest('GET', url,function (response) {
        setResultSearchInHtml(response,page, function (callback) {

            document.getElementById("search-result-container").innerHTML = callback;
                       document.getElementById("btn-next").style.visibility = "visible";
            window.scrollTo(0, 0);
        });

         if(page == 1) {
        document.getElementById("btn-back").style.visibility = "hidden";
    }

    });
}


function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement;
}

function categoryItemClick(event) {
    var target = getEventTarget(event);
    keysearch = [target.innerHTML, "2"];
    console.log(keysearch)
    //let pagenumber = document.getElementById('page-number');
    // pagenumber.innerHTML = 0;
    nextPageSearchClick(null)

};

function search(object) {
    keysearch = [object['search'].value, "1"];
    console.log(keysearch)
    //let pagenumber = document.getElementById('page-number');
    // pagenumber.innerHTML = 0;
    nextPageSearchClick(null)

}