CREATE TABLE IF NOT EXISTS users
(
    username TEXT PRIMARY KEY NOT NULL,
    email TEXT NOT NULL,
    password TEXT NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS users_email_uindex ON users (email);


CREATE TABLE IF NOT EXISTS forget_pass
(
    id SERIAL PRIMARY KEY,
    email TEXT NOT NULL,
    random_str TEXT NOT NULL,
    exp_date TIMESTAMP NOT NULL
);
CREATE UNIQUE INDEX IF NOT EXISTS forget_pass_random_str_uindex ON forget_pass (random_str);


CREATE TABLE IF NOT EXISTS categories
(
    title VARCHAR(100) PRIMARY KEY NOT NULL,
    picture VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS product
(
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    category VARCHAR(100),
    price VARCHAR(13),
    picture VARCHAR(100),
    description TEXT NULL,
    color VARCHAR(20),
    weight VARCHAR(50),
    warranty VARCHAR(60),
    manufacturer VARCHAR(50),
    year_pro VARCHAR(4),
    created_date TIMESTAMP DEFAULT now(),
    FOREIGN KEY (category) REFERENCES categories (title)
);



INSERT INTO categories (title, picture) VALUES ('لوازم جانبی', '');
INSERT INTO categories (title, picture) VALUES ('کالاهای دیجیتال', '');
INSERT INTO categories (title, picture) VALUES ('لوازم خانگی', '');
INSERT INTO categories (title, picture) VALUES ('لوازم بهداشتی', '');

INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (1, 'کیبورد', 'لوازم جانبی', '۱۵۰۰ تومان', 'keyboard.jpg', 'sa', null, null, null, null, null, '2017-07-04 21:15:50.736178');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (2, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663739');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (3, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (4, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (7, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (8, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (10, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (12, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'mouse.png', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (5, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'speaker.jpg', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (9, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'speaker.jpg', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (11, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'speaker.jpg', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
INSERT INTO product (id, name, category, price, picture, description, color, weight, warranty, manufacturer, year_pro, created_date) VALUES (6, 'موس', 'لوازم جانبی', '۵۰۰ تومان', 'speaker.jpg', 'sa', null, null, null, null, null, '2017-07-04 21:16:56.663000');
