/**
 * Created by alex on 7/6/17.
 */

var categories;
function addRowHandlers() {
    var modal = document.getElementById('myModal');

    var table = document.getElementById("result-table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler =
            function(row)
            {
                return function() {
                    var cell = row.getElementsByTagName("td")[0];
                    var id = cell.innerHTML;
                    var url = "/product/" + id + "?ajax";
                    sendHTTPRequest('GET', url,function (response) {
                        document.getElementById("modal-title").innerHTML = "ویرایش";
                        document.getElementById("btn-delete").style.visibility = "visible";

                        document.getElementById("modal-title-code").innerHTML = 'کد محصول: <span id="code">'+ response[0][0] +'</span>'

                        // document.getElementById("code").innerHTML = response[0][0];
                        document.getElementById("code-pro").value = response[0][0];
                        document.getElementById("name").value = response[0][1];
                        // document.getElementById("category").value = ;
                        document.getElementById("price").value = response[0][3];
                        document.getElementById("color").value = response[0][6];
                        document.getElementById("weight").value = response[0][7];
                        document.getElementById("warranty").value = response[0][8];
                        document.getElementById("manufacturer").value = response[0][9];
                        document.getElementById("year_pro").value = response[0][10];
                        document.getElementById("description").innerHTML = response[0][5];
                        var content_category = "";
                        categories.forEach(function (item) {
                            console.log(item)

                            content_category += '<option id="' + item +'" value="'+ item + '">' +item + '</option>';
                        });
                        document.getElementById("category").innerHTML = content_category;
                        document.getElementById(response[0][2]).selected = true;
                        modal.style.display = "block";



                    });

                };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}

function addModalHandlers() {
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    //var btn = document.getElementById("section");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal
    //btn.onclick = function() {
    //    modal.style.display = "block";
    //}

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function addRowCategoryHandlers() {
    var modal = document.getElementById('myModal');

    var table = document.getElementById("result-table");
    var rows = table.getElementsByTagName("tr");
    for (i = 0; i < rows.length; i++) {
        var currentRow = table.rows[i];
        var createClickHandler =
            function(row)
            {
                return function() {
                    document.getElementById("btn-delete").style.visibility = "visible";
                    var cell = row.getElementsByTagName("td")[0];
                    var id = cell.innerHTML;
                    document.getElementById("modal-title").innerHTML = "ویرایش";
                    // document.getElementById("modal-title-code").innerHTML = 'کد محصول: <span id="code">'+ response[0][0] +'</span>'

                    document.getElementById("name").value = id;

                    modal.style.display = "block";


                };
            };

        currentRow.onclick = createClickHandler(currentRow);
    }
}



function addOrUpdateProduct(object, cmd){
    cmmd = cmd.innerHTML;
    console.log(cmmd)
    if(cmmd == "افزودن") {
        sendHTTPRequest("POST", "/admin/products", document.getElementById("message"), object);
    }
    else    if(cmmd == "ویرایش") {
        sendHTTPRequest("PUT", "/admin/products", document.getElementById("message"), object);
    }
}

function deleteProduct(object) {
    sendHTTPRequest("DELETE", "/admin/products", document.getElementById("message"), object);

}
function openAddMenu() {
    var modal = document.getElementById('myModal');
    document.getElementById("modal-title").innerHTML = "افزودن";
    document.getElementById("btn-delete").style.visibility = "hidden";

    resetModal();
    modal.style.display = "block";
}

function openAddCategoryMenu() {
    var modal = document.getElementById('myModal');
    document.getElementById("modal-title").innerHTML = "افزودن";
    document.getElementById("btn-delete").style.visibility = "hidden";

    document.getElementById("name").value = "";
    modal.style.display = "block";
}

function addOrUpdateCategory(object, cmd){
    cmmd = cmd.innerHTML;
    console.log(cmmd)
    if(cmmd == "افزودن") {
        sendHTTPRequest("POST", "/admin/category", document.getElementById("message"), object);
    }
    else    if(cmmd == "ویرایش") {
        sendHTTPRequest("PUT", "/admin/category", document.getElementById("message"), object);
    }
}

function deleteCategory(object) {
    sendHTTPRequest("DELETE", "/admin/category", document.getElementById("message"), object);

}


function resetModal() {
    // document.getElementById("modal-title-code").parentNode.removeChild(document.getElementById("modal-title-code"));
    var content_category = "";
    categories.forEach(function (item) {
        console.log(item)

        content_category += '<option id="' + item +'" value="'+ item + '">' +item + '</option>';
    });
    document.getElementById("category").innerHTML = content_category;
    document.getElementById("modal-title-code").innerHTML = '<span id="code"></span>'
    // document.getElementById("code").innerHTML = "";
    document.getElementById("name").value = "";
    // document.getElementById("category").value = ;
    document.getElementById("price").value = "";
    document.getElementById("color").value = "";
    document.getElementById("weight").value = '';
    document.getElementById("warranty").value = "";
    document.getElementById("manufacturer").value = "";
    document.getElementById("year_pro").value = "";
    document.getElementById("description").innerHTML = "";
    document.getElementById("code-pro").value = "";



}


function getAllProducts() {

    sendHTTPRequest('GET', "/admin/products",function (response) {
        var contentHtml = "";
        try {
            response[0].forEach(function (item) {

                contentHtml += '<li> <div class="buys-item"> ' +
                    '<a href="' + response[1] + item[0] + '">' +
                    '<img src="' + response[2] + item[4] + '">' +
                    '<b>' + item[1] + '</b>' +
                    '<span>' + item[3] + '</span></a></div></li>';
            });
            object.innerHTML = page;
            document.getElementById("btn-back").style.visibility = "visible";
            document.getElementById("stuffs-contents").innerHTML = contentHtml;
            window.scrollTo(0, 0);

        }catch(TypeError)
        {
            // document.getElementById("btn-next").style.visibility = "hidden";
        }
    });
}

function getAllCategories() {
    sendHTTPRequest('GET', '/search?ajax', function (response) {

        categories =response[0]

    });
}

function prepareProductsPage(method,  url, object) {
    getHTTPPage(method,url,function (callback) {
        object.innerHTML = callback;
        addRowHandlers();
        addModalHandlers();
        getAllCategories();
    });
}

function prepareCategoriesPage(method,  url, object) {
    getHTTPPage(method,url,function (callback) {
        // console.log(callback)
        object.innerHTML = callback;
        addRowCategoryHandlers();
        addModalHandlers();
        // getAllCategories();
    });
}

