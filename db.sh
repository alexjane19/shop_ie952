#!/usr/bin/env bash

psqluser="alex"   # Database username
psqlpass="12345"  # Database password
psqldb="ie"   # Database name


#################################################
#                       #
#    PLEASE DO NOT CHANGE THE FOLLOWING CODES   #
#                       #
#################################################

#################
# Dependicies
#################
#################
# Database
#################


#sudo printf "CREATE USER $psqluser WITH PASSWORD '$psqlpass';\nDROP DATABASE IF EXISTS $psqldb;\nCREATE DATABASE $psqldb WITH OWNER $psqluser;" > database.sql

sudo -u postgres psql -f database.sql

#echo "Running postgis.sql"
#sudo -u postgres psql -d $psqldb -f /usr/share/postgresql/9.4/contrib/postgis-2.1/postgis.sql
#
#echo "Running postgis_comments.sql"
#sudo -u postgres psql -d $psqldb -f /usr/share/postgresql/9.4/contrib/postgis-2.1/postgis_comments.sql
#
#echo "Running spatial_ref_sys.sql"
#sudo -u postgres psql -d "$psqldb" -f /usr/share/postgresql/9.4/contrib/postgis-2.1/spatial_ref_sys.sql
#
#sudo -u postgres psql -d "$psqldb" -c "grant all on geometry_columns to '$psqluser';"
#sudo -u postgres psql -d "$psqldb" -c "grant all on spatial_ref_sys to '$psqluser';"

echo "Finished Database section"

python3 database_handler.py create

echo "Finished Table section"

exit