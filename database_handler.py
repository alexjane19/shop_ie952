from datetime import datetime

import psycopg2
import json
from psycopg2 import IntegrityError

class DatabaseHandler:
    def __init__(self):
        self.conn = psycopg2.connect(database="ie", user="alex", password="12345", host='127.0.0.1')
        self.cur = self.conn.cursor()
        self.query = "insert into videos_vidoal values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        self.parameters = []

    def create_tables(self):
        sql_file = open("table.sql");
        # for line in sql_file.readlines()
        lines = sql_file.readlines()
        # print(lines)
        makeitastring = ''.join(map(str, lines))
        # print(makeitastring)
        try:
            self.cur.execute(makeitastring)
            self.conn.commit()
        except psycopg2.ProgrammingError as e:
            print(e.pgerror)


    def sign_up(self,parameters):
        query = "insert into users values(%s, %s, %s)"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return False
        return True

    def login(self,parameters):
        query = "select * from users where username=%s and password=%s"
        self.cur.execute(query, parameters)
        self.conn.commit()
        row = self.cur.fetchone()
        try:
            if row[0] == parameters[0] and row[2] == parameters[1]:
                return True
            else:
                return False
        except TypeError as e:
            return False

    def change_password(self,parameters):
        query = "update users set password=%s where username=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return False
        return True

    def forget_password(self, parameters):
        query = "select * from users where email=%s"
        self.cur.execute(query, [parameters[0]])
        self.conn.commit()
        row = self.cur.fetchone()
        if row == None:
            return [False, "مشخصاتی با این ایمیل ثبت نشده است."]

        if row[1] != parameters[0]:
            return [False, "مشخصاتی با این ایمیل ثبت نشده است."]

        query = "insert into forget_pass(email, random_str, exp_date) values (%s, %s, %s)"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        # query_del = "DELETE FROM forget_pass WHERE email='{}';".format(parameters[0])
        # query_del += "DELETE FROM forget_pass WHERE exp_date < '{}';".format(datetime.now())
        # try:
        #     self.cur.execute(query_del)
        #     self.conn.commit()
        # except IntegrityError as e:
        #     print(e.pgerror)

        return [True, "لطفا به ایمیل خود مراجعه کنید."]


    def check_random_string(self, parameters):
        query = "select email from forget_pass where random_str=%s and exp_date > %s"
        self.cur.execute(query, [parameters[0], datetime.now()])
        self.conn.commit()
        row = self.cur.fetchone()
        if row == None:
            return [False, "کلید شما منقضی شده است."]

        query_del = "DELETE FROM forget_pass WHERE email='{}';".format(row[0])
        query_del += "DELETE FROM forget_pass WHERE exp_date < '{}';".format(datetime.now())
        try:
            self.cur.execute(query_del)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]

        return [True, row[0]]


    def update_password(self,parameters):
        query = "update users set password=%s where email=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        return [True]

    def add_product(self, parameters):
        query = "insert into product(name, category, price, picture, color, weight, warranty, manufacturer, year_pro, description) " \
                "values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]

        return [True, "کالا با موفقیت ثبت شد."]

    def get_product(self, parameters):
        query = "select id,name,category,price,picture,description,color,weight,warranty,manufacturer,year_pro from product where id=%s"
        self.cur.execute(query, parameters)
        self.conn.commit()
        row = self.cur.fetchone()
        if row == None:
            return [False, "چنین محصولی وجود ندارد."]
        return [True, row]

    def get_products(self,parameters):
        query = "select id,name,category,price,picture,color,weight,warranty,manufacturer,year_pro from product where category=%s  ORDER BY created_date OFFSET %s LIMIT 10"
        self.cur.execute(query, parameters)
        self.conn.commit()
        products =[]
        for row in self.cur.fetchall():
            products.append(row)

        if len(products) == 0:
            return [False, "کالایی در این دسته موجود نمی باشد."]
        return [True, products]

    def update_product(self,parameters):
        query = "update product set name=%s, category=%s, price=%s, picture=%s, color=%s, weight=%s, warranty=%s, manufacturer=%s, year_pro=%s, description=%s where id=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        return [True, "بروزرسانی با موفقیت انجام شد."]

    def delete_product(self,parameters):
        query = "DELETE FROM product WHERE id=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        return [True, "حذف با موفقیت انجام شد."]

    def add_category(self, parameters):
        query = "insert into categories(title, picture) " \
                "values (%s, %s)"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]

        return [True, "دسته با موفقیت ثبت شد."]

    def update_category(self, parameters):
        query = "update categories set title=%s picture=%s where title=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        return [True, "بروزرسانی با موفقیت انجام شد."]

    def delete_category(self, parameters):
        query = "DELETE FROM categories WHERE title=%s"
        try:
            self.cur.execute(query, parameters)
            self.conn.commit()
        except IntegrityError as e:
            print(e.pgerror)
            return [False, e.pgerror]
        return [True, "حذف با موفقیت انجام شد."]

    def get_categories(self):
        query = "select title from categories"
        self.cur.execute(query)
        self.conn.commit()
        categories = []
        for row in self.cur.fetchall():
            categories.append(row)

        # if len(categories) == 0:
        #     return [False, "کالایی یافت نشد."]

        return [True, categories]


    def search_by_name(self,parameters):
        query = "select id,name,category,price,picture,color,weight,warranty,manufacturer,year_pro from product where name like %s  ORDER BY created_date OFFSET %s LIMIT 10"

        self.cur.execute(query,["%"+parameters[0]+"%", parameters[1]])
        self.conn.commit()
        products = []
        for row in self.cur.fetchall():
            products.append(row)
        print(len(products))

        if len(products) == 0:
            return [False, "کالایی یافت نشد."]

        return [True, products]

    def search_by_category(self,parameters):
        query = "select id,name,category,price,picture,color,weight,warranty,manufacturer,year_pro from product where category like %s  ORDER BY created_date OFFSET %s LIMIT 10"

        self.cur.execute(query,["%"+parameters[0]+"%", parameters[1]])
        self.conn.commit()
        products = []
        for row in self.cur.fetchall():
            products.append(row)

        if len(products) == 0:
            return [False, "کالایی یافت نشد."]

        return [True, products]


    def get_all_products(self):
        query = "select id,name,category,price,picture,color,weight,warranty,manufacturer,year_pro from product  ORDER BY created_date"
        self.cur.execute(query)
        self.conn.commit()
        products = []
        for row in self.cur.fetchall():
            products.append(row)

        if len(products) == 0:
            return [False, products]
        return [True, products]

    def executor(self):
        # try:
            self.cur.execute(self.query,self.parameters)
            self.conn.commit()
        # except psycopg2.IntegrityError as e:
        #     file_handler = FileHandler("db_logs.txt")
        #     file_handler.write("query=" + self.query)
        #     file_handler.write("parameters=" + str(self.parameters))
        #     file_handler.write(str(e))
        # except psycopg2.DataError as e:
        #     file_handler = FileHandler("db_logs.txt")
        #     file_handler.write("query=" + self.query)
        #     file_handler.write("parameters=" + str(self.parameters))
        #     file_handler.write(str(e))

    def close(self):
        self.cur.close()
        self.conn.close()

    def set_query(self,query):
        self.query = query

    def prepare_query(self,fields):
        self.parameters = fields
        # sub_query = ""
        # for field in fields:
        #     sub_query += '"' + field + '",'
        # self.parameters = (sub_query)

    def select_query(self,parameter):
        self.query = "select title, direct_link, duration from videos_result_search where id = %s"
        self.parameters = [ parameter]
        self.executor()
        row = self.cur.fetchone()
        val = [row[0],json.loads(row[1]), row[2]]
        return val


import sys
if len(sys.argv)>1:
    if sys.argv[1] == "create":
        db_handler = DatabaseHandler()
        db_handler.create_tables()
        print("Create Tables finished.")

# test = DatabaseHandler();
# test.add_product(['qrwqw', 'لوازم جانبی', 'qeqw', 'mouse.png', '123', 'eqwe', 'ad', '', '', ''])
# r =test.search_by_name(["مو",1])
# print(r)
# test.sign_up(["alex12", "e1@gmaul2.com", "12345"])
# print(test.login(["alex", "12345"]))

# string = json.dumps(lst)
# lst = json.loads(string)

# def share_user_photo(_person):
#   _photo = Photo()
#   _photo.insert(_person.getUserId())
#   _qurey = "insert into photo values('"+_photo.getUserId()+"', '"+_photo.getPhotoId()+"', '"+_photo.getWriting()+"', '"\
#            +_photo.getAccessLevel()+"', '"+_photo.getDate()+"');"
#   db = qurey_in_database(_qurey)
#   close_database(db)

# def get_following(_person):
#   _qurey="select * from follow where userid='"+_person.getUserId()+"';"
#   db = qurey_in_database(_qurey)
#   _index=0
#   _following=[]
#   for a in db['cursor'].fetchall():
#     _following.append(Follow(userid = a[0], followuserid = a[1], date = a[2]))
#     for l1 in a:
#       print(l1, end=' ');
#     print("")
#     _index+=1
#   close_database(db)
#   return _following;


